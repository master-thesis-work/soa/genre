create table genre
(
    id   serial
        constraint genre_pk
            primary key,
    name varchar
);

alter table genre
    owner to postgres;

create unique index genre_id_uindex
    on genre (id);

create unique index genre_name_uindex
    on genre (name);

