package http

import (
	"genre/internal/database"
	"genre/internal/managers"
	healthCheck "github.com/RaMin0/gin-health-check"
	"github.com/gin-gonic/gin"
	"log"
)

type server struct {
	router *gin.Engine
	db     database.DataStore
}

func NewServer(db database.DataStore) *server {
	return &server{
		router: gin.New(),
		db:     db,
	}
}

func (srv *server) setupRouter() {
	srv.router.Use(gin.Recovery())
	srv.router.Use(healthCheck.Default())

	v1 := srv.router.Group("/v1")

	genreManager := managers.NewGenre(srv.db.GenreRepository())
	NewGenre(genreManager).Init(v1)
}

func (srv *server) Run() {
	srv.setupRouter()

	if err := srv.router.Run(":8080"); err != nil {
		log.Fatal(err)
	}
}
