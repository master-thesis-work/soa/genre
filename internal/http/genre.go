package http

import (
	"genre/internal/managers"
	"genre/pkg/models"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

type genre struct {
	genreManager managers.Genrer
}

func NewGenre(genreManager managers.Genrer) *genre {
	return &genre{
		genreManager: genreManager,
	}
}

func (g *genre) Init(router *gin.RouterGroup) {
	route := router.Group("/genre")

	route.GET("/genres", g.genres)
	route.POST("", g.createGenre)
	route.GET("", g.genre)
	route.DELETE("/:id", g.deleteGenre)
}

func (g *genre) genres(ctx *gin.Context) {
	page := ctx.Query("page")
	limit := ctx.Query("limit")

	response, err := g.genreManager.Genres(models.HandlePagination(limit, page))
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.CustomResponse{
			Code:    http.StatusInternalServerError,
			Message: err.Error(),
		})

		return
	}

	if len(response) == 0 {
		ctx.JSON(http.StatusNotFound, models.CustomResponse{
			Code:    http.StatusNotFound,
			Message: http.StatusText(http.StatusNotFound),
		})

		return
	}

	ctx.JSON(http.StatusOK, response)
}

func (g *genre) createGenre(ctx *gin.Context) {
	requestBody := new(models.Genre)

	if err := ctx.BindJSON(requestBody); err != nil {
		ctx.JSON(http.StatusBadRequest, models.CustomResponse{Code: http.StatusBadRequest, Message: http.StatusText(http.StatusBadRequest)})

		return
	}

	err := g.genreManager.AddGenre(requestBody)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.CustomResponse{
			Code:    http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	ctx.JSON(http.StatusOK, http.StatusText(http.StatusOK))
}

func (g *genre) genre(ctx *gin.Context) {
	idParam := ctx.Query("id")

	if idParam == "" {
		ctx.JSON(http.StatusBadRequest, models.CustomResponse{Code: http.StatusBadRequest, Message: http.StatusText(http.StatusBadRequest)})

		return
	}

	var id int

	var err error

	if idParam != "" {
		id, err = strconv.Atoi(idParam)
		if err != nil {
			ctx.JSON(http.StatusBadRequest, models.CustomResponse{Code: http.StatusBadRequest, Message: http.StatusText(http.StatusBadRequest)})

			return
		}
	}

	response, err := g.genreManager.Genre(id)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.CustomResponse{
			Code:    http.StatusInternalServerError,
			Message: err.Error(),
		})

		return
	}

	ctx.JSON(http.StatusOK, response)
}

func (g *genre) deleteGenre(ctx *gin.Context) {
	idParam := ctx.Param("id")

	id, err := strconv.Atoi(idParam)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, models.CustomResponse{Code: http.StatusBadRequest, Message: http.StatusText(http.StatusBadRequest)})

		return
	}

	err = g.genreManager.DeleteGenre(id)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.CustomResponse{
			Code:    http.StatusInternalServerError,
			Message: err.Error(),
		})

		return
	}

	ctx.JSON(http.StatusOK, http.StatusText(http.StatusOK))
}
