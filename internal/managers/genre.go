package managers

import (
	"genre/internal/database"
	"genre/pkg/models"
)

type Genrer interface {
	AddGenre(genre *models.Genre) error
	DeleteGenre(id int) error
	Genres(page, limit int) ([]*models.Genre, error)
	Genre(id int) (*models.Genre, error)
}

type Genre struct {
	genreRepository database.Genrer
}

func NewGenre(genreRepository database.Genrer) Genrer {
	return &Genre{
		genreRepository: genreRepository,
	}
}

func (g *Genre) AddGenre(genre *models.Genre) error {
	return g.genreRepository.AddGenre(genre)
}

func (g *Genre) DeleteGenre(id int) error {
	return g.genreRepository.DeleteGenre(id)
}

func (g *Genre) Genres(page, limit int) ([]*models.Genre, error) {
	return g.genreRepository.Genres(page, limit)
}

func (g *Genre) Genre(id int) (*models.Genre, error) {
	return g.genreRepository.Genre(id)
}
