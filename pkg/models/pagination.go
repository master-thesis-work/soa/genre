package models

import "strconv"

func HandlePagination(limitStr, pageStr string) (int, int) {
	const defaultPage = 1
	const defaultLimit = 10

	var limit, page int

	page, err := strconv.Atoi(pageStr)
	if err != nil {

		page = defaultPage
	}

	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		limit = defaultLimit
	}

	if limit > 100 {
		limit = defaultLimit
	}

	return (page - 1) * limit, limit
}
